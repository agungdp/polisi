<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta property="og:title" content="idEA - Indonesian E-Commerce Association"/>
	<meta property="og:type"  content="article" />
	<meta property="og:site_name" content="idEA"/>
	<meta property="og:url" content="https://idea.or.id/"/>
	<meta property="og:image" content="https://idea.or.id/assets/images/idea2015-logo-share.jpg"/>
	<meta property="og:description" content="Asosiasi E-Commerce Indonesia (idEA - Indonesian E-Commerce Association) merupakan wadah komunikasi antar pelaku industri E-Commerce Indonesia. idEA resmi didirikan pada bulan Mei 2012 di Jakarta"/>
	    <title>AP2HNI| ASOSIASI PURNAWIRA PENEGAK HUKUM NARKOTIKA INDONESIA</title>
	<meta name="robots" content="index, follow" />
	<meta name="description" content="Asosiasi E-commerce Indonesia" /><!--156-->
	<meta name="keywords" content="idea, ecommerce" /> <!-- 6 koma -->
	<meta http-equiv="Copyright" content="idEA" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="content-language" content="id">
	<meta name="revisit-after" content="1 days" />
	<meta name="webcrawlers" content="all" />
	<meta name="rating" content="general" />
	<meta name="spiders" content="all" />
	<meta name="geo.region" content="ID" />
	<meta name="geo.placename" content="Jakarta" />
	<meta name="geo.position" content="-6.211544;106.845172" />
	<meta name="ICBM" content="-6.211544, 106.845172" />
	<link rel="canonical" href="https://www.idea.or.id/foto" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="https://idea.or.id/rss.xml" />
	<link rel="shortcut icon" href="../gambar/icon.ico" />
    <!-- Bootstrap -->
    <link type="text/css" href="https://www.idea.or.id/assets/css/bootstrap.min.css" rel="stylesheet"> 
    <link type="text/css" href="https://www.idea.or.id/assets/css/font-awesome.min.css" rel="stylesheet">
	<link type="text/css" href="https://www.idea.or.id/assets/css/animate.css" rel="stylesheet">
	<link type="text/css" href="https://www.idea.or.id/assets/css/main.css" rel="stylesheet">
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-36717887-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>
  </head>
  <body>
	<!-- header -->
     
																									<!-- header -->
<div class="container-fluid">
	<div class="row">
		<div class='col-md-12 col-sm-12 col-xs-12 nopadding wrapmenu'>
			
			<a id="nav-toggle"><span></span></a>
			<div class='col-md-10 col-sm-10 col-xs-12 nopadding'>
				<nav>
				<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxmenu'>	
					
					<div class='dropdowns col-md-12 col-sm-12 col-xs-12 nopadding'>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.2s">
							<a class='parent'>Beranda <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.3s">
							<a class='parent'>Tentang Kami <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.3s">
							<a class='parent'>Kemitraan <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.4s">
							<a class='parent'>Gallery <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.4s">
							<a href='' class='parent'>Hubungi Kami</a>
						</div>
						
					</div>
				</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
		<i class="close fa fa-close" data-dismiss="modal" aria-label="Close" style='font-size:22px; margin-top:2px;'></i>
        <h4 class="modal-title" id="myModalLabel">Masuk</h4>
      </div>
	  <form id="loginForm">
      <div class="modal-body">
			<div class="form-group">
				<label for="username">Nama Website</label>
				<input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="domain.co.id" required>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" name="password" id="password" autocomplete="off" required>
			</div>
			<a href='' data-toggle="modal" data-target="#myModal1" data-dismiss="modal">Forget Password</a>
      </div>
      <div class="modal-footer">
			<a href='' class="btn btn-default">Gabung</a>
			<button type="submit" class="btn btn-primary">Masuk</button>
      </div>
	  </form>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i class="close fa fa-close" data-dismiss="modal" aria-label="Close" style='font-size:22px; margin-top:2px;'></i>
				<h4 class="modal-title" id="myModalLabel">Forget Password</h4>
				</div>
				<form id="forgotForm">
						<div class="modal-body">
						<div class="form-group">
							<label for="txt_forgot">Nama Website</label>
							<input type="text" class="form-control" name="txt_forgot" id="txt_forgot" autocomplete="off" required>
						</div>
						</div>
						<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Reset Password</button>
					</div>
				</form>
		</div>
	</div>
</div>
<!-- end header -->	<!-- end header -->
	
	<!--content-->
		<div class='container-fluid'>
		<div class='row'>
			<div class='col-md-12 nopadding bannerwrap'></div>
		</div>
    	<div class='row wrap'>
		<div class='col-md-12 col-sm-12 col-xs-12'>
			<div class='col-md-3 col-sm-3 col-xs-12 boxshadowleft1 boxallwrap'>
				<div class='col-md-12 col-sm-12 col-xs-12 box'>
					<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxtitle'>
						<div class='col-md-12 col-sm-12 col-xs-12 nopadding'>
						<span class='spanboxtitle'>
								 VIDEO TERBARU 						</span>
						</div>
					</div>
					
					<div class='col-md-12 col-sm-12 col-xs-12 boxlist'>
														<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxcontentlist'>
								<img src='../gambar/1.jpg' class='img-responsive' style='display:inline-block;'>
								<a class='galvarious' href="">
									<div class='boxtitlelist'>
										<div class='titlelistnewsevent'>Peresmian Interoperabilitas Sistem Pendaftaran PSE Kemkominfo dan idEA</div>
									</div>
								</a>
							</div>
														<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxcontentlist'>
								<img src='../gambar/2.jpg' class='img-responsive' style='display:inline-block;'>
								<a class='galvarious' href="">
									<div class='boxtitlelist'>
										<div class='titlelistnewsevent'>Pemaparan Road Map E-Commerce Ibu Mira Tayyiba</div>
									</div>
								</a>
							</div>
														<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxcontentlist'>
								<img src='../gambar/3.jpg' class='img-responsive' style='display:inline-block;'>
								<a class='galvarious' href="">
									<div class='boxtitlelist'>
										<div class='titlelistnewsevent'>Sambutan Adimarta - Ketua Bidang Keanggotaan dan Organisasi idEA</div>
									</div>
								</a>
							</div>
														
					</div>
				</div>
			</div>
			<div class='col-md-9 col-sm-9 col-xs-12 nopadding boxshadowleft2'>
				<div class='col-md-12 col-md-12 col-xs-12 box'>
					<div class='col-md-12 col-md-12 col-xs-12 boxtitle'>
						<span class='spanboxtitle'>
								 GALERI FOTO 						</span>
					</div>
					
					<div class='col-md-12 col-sm-12 col-xs-12 nopadding'>
						<div class="cycle-slideshow" 
							data-cycle-fx=scrollHorz
							data-cycle-timeout=0
							data-cycle-pager="#no-template-pager"
							data-cycle-pager-template=""
							>
														<img src="" class='img-responsive'>
														<img src="" class='img-responsive'>
														<img src="" class='img-responsive'>
														<img src="" class='img-responsive'>
													</div>
						<div id='no-template-pager' class='col-md-12 col-sm-12 col-xs-12 nopadding mt15 mb15'>
														<div class='col-md-3 col-sm-3 col-xs-3 pl0 pr0'> 
								<img src="" class='img-responsive'>
							</div>
														<div class='col-md-3 col-sm-3 col-xs-3 pl0 pr0'> 
								<img src="" class='img-responsive'>
							</div>
														<div class='col-md-3 col-sm-3 col-xs-3 pl0 pr0'> 
								<img src="" class='img-responsive'>
							</div>
														<div class='col-md-3 col-sm-3 col-xs-3 pl0 pr0'> 
								<img src="" class='img-responsive'>
							</div>
													</div>
					</div>
					<div class='col-md-12 col-sm-12 col-xs-12 boxtitle' style='margin-bottom:10px;'>
						<span class='spanboxtitle'>
								 ALBUM FOTO 						</span>
					</div>
					
					<div id='result' class='col-md-12 col-sm-12 col-xs-12 nopadding boxallwrap'>
						
					</div>
					
				</div>
			</div>
		</div>
		</div>
	</div>
	<!--end content-->
	
	<!-- footer -->
	<!-- footer -->

<!-- end footer -->
	<!-- end footer -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://www.idea.or.id/assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://www.idea.or.id/assets/js/bootstrap.min.js"></script>
	<script src="https://www.idea.or.id/assets/js/wow.min.js"></script>
	<script>
    new WOW().init();
	
	$(function(){

	var w = $(window).width();
	if(w > 769){
			$("#boxallhomevideoclone").detach().appendTo('#boxallhomevideo');
			$(".dropdowns .subdropdowns").hover(function(e){
			e.preventDefault(); 
			    clearTimeout($.data(this, 'timer'));
				$('div:first', this).stop(true, true).fadeIn(200);
			}, function(){
				$.data(this, 'timer', setTimeout($.proxy(function() {
					$('div:first', this).stop(true, true).fadeOut(200);
				}, this), 200));
			});
	} else {
			$("#boxallhomevideo").detach().appendTo('#boxallhomevideoclone');
	
			$(".dropdowns .subdropdowns").click(function(){
				$(this).toggleClass('active');
				if($(this).hasClass('active')) {
					$('.parent i',this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
				} else {
					$('.parent i',this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
				}
				$('div:first',this).slideToggle();
			});
	}
	
	});	
	
	$(function() {
		var pull 		= $('#nav-toggle');
			menu 		= $('nav');
			menuHeight	= menu.height();


		$(pull).on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
			menu.slideToggle();
			$(".subdropdowns").addClass("wow slideInLeft");
			new WOW().init();
		});

		$(window).resize(function(){
		var w = $(window).width();
		if(w > 769 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
		});
	});
    </script>
	<script type="text/javascript">
	$(document).ready(function(){
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }

		$("#loginForm").on("submit", function(e){
		e.preventDefault();
		var fd =  $("#loginForm").serialize();
		$.ajax({
				type : "POST",
				url : "https://www.idea.or.id/login",
				data : fd + (getUrlParameter('login') === 'pse' ? '&client=pse_idea_or_id_client_id' : ''),
				dataType: 'json',
				success : function (data, status)
				{
					if(data.valid == true) {
						if(data.token) {
                            location.href = 'http://pse.idea.or.id/pendaftaran?token=' + data.token;
						} else {
                            /*if(sessionpse == 'true'){
                                location.href = 'https://www.idea.or.id/pse';
                            } else {*/
                                location.href = 'https://www.idea.or.id/registrasi';
                            //}
						}
					} else {
						alert(data.message);   
						/*if(data.field != '-nan')
						{
						  $("#"+data.field).focus();
						}*/
					}				
				},
				error: function(jqXHR, textStatus, errorMessage) {
					   console.log(errorMessage); // Optional
				}
			});
		});
		
		$("#forgotForm").on("submit", function(e){
		e.preventDefault();
		$.ajax({
                type	       	: "POST",
                url            	: "https://www.idea.or.id/login/forgot",
                dataType       	: 'json',
                data           	: {
					'login_account'	: $('#txt_forgot').val()
                },
                error: function()
                {
                    alert('error when resetting password');
                },
                success  : function (data)
                {
		    if(data.status == 'success')
		    {
				alert('link untuk mereset password telah dikirimkan ke email anda\n\nBila anda belum menerima email konfirmasi pastikan sudah mengecek folder junk/spam')
		    } else {
				alert(data.status);
		    }
                }
            });; 
		});
		
	});
	</script>
	<script src="https://www.idea.or.id/assets/js/jquery.cycle2.js"></script>
<script src="https://www.idea.or.id/assets/js/jquery.cycle2.swipe.js"></script>
<script src="https://www.idea.or.id/assets/js/ios6fix.js"></script>
<script type="text/javascript">
$(document).ready(function() { 
    load_reviews();
});

function load_reviews() { 
    $.ajax ({ 
    url : 'https://www.idea.or.id/foto/load_reviews/', 
    type : 'POST', 
    error : function(xhr, ajaxOptions, thrownError) { alert('(Error from Load Review event load) AJAX Error : ' + xhr.status + ' ' + thrownError); 
    return false; 
    }, 
    success : function (data) { 
    $('#result').html(data); 
    return false; 
    } 
    }); 
}
</script>
  </body>
</html>
