<footer class="page-footer green center-on-small-only col-lg-12 col-md-12 col-xl-12 col-sm-12 col-xs-12">
  <div class="container">
    <div class="row">
      <div class="col l3 s12">
        <h5 class="white-text">Alamat Kami</h5>
        <h5>MENARA GLOBAL LANTAI.3
			Jl. Gatot Subroto Kav.27 RT.003/ RW.03
			Kuningan Timur, Jakarta Selatan,
			DKI Jakarta - 12950
		</h5>
        


      </div>
	  <div class="col l3 s12">
       
         <h5 class="white-text">Maps</h5>
			 <div id="map_canvas" style="margin-bottom:1em;"></div>
			 
		


      </div>
	 
      <div class="col l3 s12">
        <h5 class="white-text">Live Tweet</h5>
        <ul>
          <li>
           <a href="https://twitter.com/intent/tweet?screen_name=agungbudipraset&ref_src=twsrc%5Etfw" class="twitter-mention-button" data-show-count="false">Tweet to @agungbudipraset</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          
		  </li>
        </ul>
		<h5 class="white-text">Visitor</h5>
       
      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Hubungi Kami</h5>
        <ul>
          <li>
			<ul>
					<a class="white-text" href="#!">
			</ul>
			<ul>
				<i class="fa fa-twitter"></i>&nbsp;Twitter :
			</ul>
            <ul>
				<i class="fa fa-facebook-official"></i>&nbsp;Facebook :
			</ul>
			<ul>
				<i class="fa fa-instagram"></i>&nbsp;Instagram :
			</ul>
			<ul>
				<i class="fa fa-youtube-play"></i>&nbsp;Youtube :
			</ul>
			<ul>
				<i class="fa fa-phone"></i>&nbsp;Telepon :
			</ul>
			<ul>
				<i class="fa fa-envelope"></i>&nbsp;Email :
			</ul>
	
			</a>
          </li>

        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container white-text text-lighten-3" style="font-weight:bold">
      <center>
        &copy COPYRIGHT AND DESIGNED BY
        <a class="white-text text-lighten-3" href="https://laravel.themefactory.net">SANGGAR MERAH PUTIH</a>.
        
      </center>
    </div>
  </div>
</footer>