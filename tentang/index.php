<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta property="og:title" content="idEA - Indonesian E-Commerce Association"/>
	<meta property="og:type"  content="article" />
	<meta property="og:site_name" content="idEA"/>
	<meta property="og:url" content="https://idea.or.id/"/>
	<meta property="og:image" content="https://idea.or.id/assets/images/idea2015-logo-share.jpg"/>
	<meta property="og:description" content="Asosiasi E-Commerce Indonesia (idEA - Indonesian E-Commerce Association) merupakan wadah komunikasi antar pelaku industri E-Commerce Indonesia. idEA resmi didirikan pada bulan Mei 2012 di Jakarta"/>
	    <title>AP2HNI| ASOSIASI PURNAWIRA PENEGAK HUKUM NARKOTIKA INDONESIA</title>
	<meta name="robots" content="index, follow" />
	<meta name="description" content="Asosiasi E-commerce Indonesia" /><!--156-->
	<meta name="keywords" content="idea, ecommerce" /> <!-- 6 koma -->
	<meta http-equiv="Copyright" content="idEA" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="content-language" content="id">
	<meta name="revisit-after" content="1 days" />
	<meta name="webcrawlers" content="all" />
	<meta name="rating" content="general" />
	<meta name="spiders" content="all" />
	<meta name="geo.region" content="ID" />
	<meta name="geo.placename" content="Jakarta" />
	<meta name="geo.position" content="-6.211544;106.845172" />
	<meta name="ICBM" content="-6.211544, 106.845172" />
	<link rel="canonical" href="https://www.idea.or.id/foto" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="https://idea.or.id/rss.xml" />
	<link rel="shortcut icon" href="../gambar/icon.ico" />
    <!-- Bootstrap -->
    <link  href="../css/bootstrap.css" rel=" stylesheet"/>
    <link type="text/css" href="../css/font-awesome.min.css" rel="stylesheet">
	<link type="text/css" href="../css/animate.css" rel="stylesheet">
	<link type="text/css" href="../css/main.css" rel="stylesheet">
	<script src="../js/jquery-1.12.4.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-36717887-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>
	<link rel="stylesheet" href="../js/jquery/ui-lightness/jquery-ui-1.10.2.custom.css" />
    <script type="text/javascript" src="../js/jquery/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../js/jquery/jquery-ui-1.10.2.custom.min.js"></script>

    <script type="text/javascript" src="../js/primitives.min.js"></script>
    <link href="../css/primitives.latest.css" media="screen" rel="stylesheet" type="text/css" />

    <script type='text/javascript'>//<![CDATA[ 
        $(window).load(function () {
            var options = new primitives.orgdiagram.Config();

            var items = [
                new primitives.orgdiagram.ItemConfig({
                    id: 0,
                    parent: null,
                    title: "Kepala Sekolah",
                    description: "Nama Kepala Sekolah",
                    image: "images/photos/a.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 1,
                    parent: 0,
                    title: "Wakasek Manajemen Mutu",
                    description: "Nama Kesiswaan",
                    image: "images/photos/b.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 2,
                    parent: 0,
                    title: "Wakasek Kurikulum",
                    description: "Nama Humas",
                    image: "images/photos/b.png"
                }),
				  new primitives.orgdiagram.ItemConfig({
                    id: 3,
                    parent: 0,
                    title: "Wakasek kesiswaan",
                    description: "Nama Humas",
                    image: "images/photos/b.png"
                }),
				  new primitives.orgdiagram.ItemConfig({
                    id: 4,
                    parent: 0,
                    title: "Wakasek Sarana Prasarana",
                    description: "Nama Humas",
                    image: "images/photos/b.png"
                }),
				new primitives.orgdiagram.ItemConfig({
                    id: 5,
                    parent: 0,
                    title: "Wakasek Sarana Prasarana",
                    description: "Nama Humas",
                    image: "images/photos/b.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 6,
                    parent: 1,
                    title: "Pembina Osis",
                    description: "Nama Pembina Osis",
                    image: "images/photos/c.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 7,
                    parent: 1,
                    title: "Pembina Ekstra",
                    description: "Pembina Ekstra",
                    image: "images/photos/c.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 8,
                    parent: 2,
                    title: "UKS",
                    description: "Nama UKS",
                    image: "images/photos/c.png"
                }),
                new primitives.orgdiagram.ItemConfig({
                    id: 9,
                    parent: 2,
                    title: "Koperasi",
                    description: "Nama Koperasi",
                    image: "images/photos/c.png"
                })
            ];

            options.items = items;
            options.cursorItem = 0;
            options.hasSelectorCheckbox = primitives.common.Enabled.False;

            jQuery("#strukturorganisasi").orgDiagram(options);
        });//]]>  

    </script>
  </head>
  <body>
	<!-- header -->
     
																									<!-- header -->
<div class="container-fluid">
	<div class="row">
		<div class='col-md-12 col-sm-12 col-xs-12 nopadding wrapmenu'>
			
			<a id="nav-toggle"><span></span></a>
			<div class='col-md-10 col-sm-10 col-xs-12 nopadding'>
				<nav>
				<div class='col-md-12 col-sm-12 col-xs-12 nopadding boxmenu'>	
					
					<div class='dropdowns col-md-12 col-sm-12 col-xs-12 nopadding'>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.2s">
							<a class='parent'>Beranda <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.3s">
							<a class='parent'>Tentang Kami <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.3s">
							<a class='parent'>Kemitraan <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.4s">
							<a class='parent'>Gallery <i class="fa fa-chevron-down hidden-sm hidden-md visible-xs" style='position:absolute; top:12px; right:10px;'></i></a>
							
						</div>
						<div class='subdropdowns col-md-2 col-sm-2 col-xs-12' data-wow-duration="0.3s" data-wow-delay="0.4s">
							<a href='' class='parent'>Hubungi Kami</a>
						</div>
						
					</div>
				</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->




<!-- end header -->	<!-- end header -->
	
	<!--content-->
		<div class='container-fluid'>
		<div class='row'>
			<div class='col-md-12 nopadding bannerwrap'></div>
		</div>
    	<div class='row wrap'>
			<div class='col-md-12 col-sm-12 col-xs-12'>
				

			</div>
		</div>
		 <center>
		<div class="col-md-12" style="margin-top:-380px;height:100%;" id="tabs"><!--kanan !-->
			<h3 style="color:black; text-shadow: 0.1em 0.1em 0.2em rgb(119, 119, 119);">
								<br />
									
										<center>
											PROFIL SEKOLAH SMK SWAKARYA PALEMBANG
										</center>
									<hr />
							</h3>
	
  <ul>
    <li><a href="#tabs-1">Struktur Organisasi</a></li>
    <li><a href="#tabs-2">Keadaan Yayasan</a></li>
	<li><a href="#tabs-3">Sarana</a></li>
	<li><a href="#tabs-4">Jumlah Kelas</a></li>
	<li><a href="#tabs-5">Jumlah Siswa</a></li>
    <li><a href="#tabs-6">Data Guru</a></li>
	<li><a href="#tabs-7">Fasilitas Lainnya</a></li>
  </ul>
  
  <div id="tabs-1">
    
		<div id="strukturorganisasi" style="width: 640px; height: 480px; border-style: dotted; border-width: 1px;" />
	
  </div>
  <div id="tabs-2">
     
  </div>
  

            </div>
	</center>
	</div>
	<!--end content-->
	
	<!-- footer -->
	<!-- footer -->

<!-- end footer -->
	<!-- end footer -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://www.idea.or.id/assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://www.idea.or.id/assets/js/bootstrap.min.js"></script>
	<script src="https://www.idea.or.id/assets/js/wow.min.js"></script>
	<script>
    new WOW().init();
	
	$(function(){

	var w = $(window).width();
	if(w > 769){
			$("#boxallhomevideoclone").detach().appendTo('#boxallhomevideo');
			$(".dropdowns .subdropdowns").hover(function(e){
			e.preventDefault(); 
			    clearTimeout($.data(this, 'timer'));
				$('div:first', this).stop(true, true).fadeIn(200);
			}, function(){
				$.data(this, 'timer', setTimeout($.proxy(function() {
					$('div:first', this).stop(true, true).fadeOut(200);
				}, this), 200));
			});
	} else {
			$("#boxallhomevideo").detach().appendTo('#boxallhomevideoclone');
	
			$(".dropdowns .subdropdowns").click(function(){
				$(this).toggleClass('active');
				if($(this).hasClass('active')) {
					$('.parent i',this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
				} else {
					$('.parent i',this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
				}
				$('div:first',this).slideToggle();
			});
	}
	
	});	
	
	$(function() {
		var pull 		= $('#nav-toggle');
			menu 		= $('nav');
			menuHeight	= menu.height();


		$(pull).on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
			menu.slideToggle();
			$(".subdropdowns").addClass("wow slideInLeft");
			new WOW().init();
		});

		$(window).resize(function(){
		var w = $(window).width();
		if(w > 769 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
		});
	});
    </script>
	<script type="text/javascript">
	$(document).ready(function(){
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }

		$("#loginForm").on("submit", function(e){
		e.preventDefault();
		var fd =  $("#loginForm").serialize();
		$.ajax({
				type : "POST",
				url : "https://www.idea.or.id/login",
				data : fd + (getUrlParameter('login') === 'pse' ? '&client=pse_idea_or_id_client_id' : ''),
				dataType: 'json',
				success : function (data, status)
				{
					if(data.valid == true) {
						if(data.token) {
                            location.href = 'http://pse.idea.or.id/pendaftaran?token=' + data.token;
						} else {
                            /*if(sessionpse == 'true'){
                                location.href = 'https://www.idea.or.id/pse';
                            } else {*/
                                location.href = 'https://www.idea.or.id/registrasi';
                            //}
						}
					} else {
						alert(data.message);   
						/*if(data.field != '-nan')
						{
						  $("#"+data.field).focus();
						}*/
					}				
				},
				error: function(jqXHR, textStatus, errorMessage) {
					   console.log(errorMessage); // Optional
				}
			});
		});
		
		$("#forgotForm").on("submit", function(e){
		e.preventDefault();
		$.ajax({
                type	       	: "POST",
                url            	: "https://www.idea.or.id/login/forgot",
                dataType       	: 'json',
                data           	: {
					'login_account'	: $('#txt_forgot').val()
                },
                error: function()
                {
                    alert('error when resetting password');
                },
                success  : function (data)
                {
		    if(data.status == 'success')
		    {
				alert('link untuk mereset password telah dikirimkan ke email anda\n\nBila anda belum menerima email konfirmasi pastikan sudah mengecek folder junk/spam')
		    } else {
				alert(data.status);
		    }
                }
            });; 
		});
		
	});
	</script>
	<script src="https://www.idea.or.id/assets/js/jquery.cycle2.js"></script>
<script src="https://www.idea.or.id/assets/js/jquery.cycle2.swipe.js"></script>
<script src="https://www.idea.or.id/assets/js/ios6fix.js"></script>
<script type="text/javascript">
$(document).ready(function() { 
    load_reviews();
});

function load_reviews() { 
    $.ajax ({ 
    url : 'https://www.idea.or.id/foto/load_reviews/', 
    type : 'POST', 
    error : function(xhr, ajaxOptions, thrownError) { alert('(Error from Load Review event load) AJAX Error : ' + xhr.status + ' ' + thrownError); 
    return false; 
    }, 
    success : function (data) { 
    $('#result').html(data); 
    return false; 
    } 
    }); 
}
</script>
  </body>
</html>
